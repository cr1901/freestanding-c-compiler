CC=gcc
CFLAGS=-g -ansi -pedantic -Wall -Wextra -I.
AS=
ASFLAGS=
LD=gcc
LDFLAGS=
RM=rm
CP=cp

#Executable Link
afcc/afcc: afcc/afcc.o  headers/limits.o  headers/float.o  lexer/tokens.o  parser/interface.o  
	$(LD) $(LDFLAGS) -o afcc/afcc  afcc/afcc.o  headers/limits.o  headers/float.o  lexer/tokens.o  parser/interface.o  

#C Sources- AFCC
afcc/afcc.o: afcc/afcc.c
	$(CC) $(CFLAGS) -c -o afcc/afcc.o afcc/afcc.c
headers/limits.o: headers/limits.c
	$(CC) $(CFLAGS) -c -o headers/limits.o headers/limits.c
headers/float.o: headers/float.c
	$(CC) $(CFLAGS) -c -o headers/float.o headers/float.c
lexer/tokens.o: lexer/tokens.c
	$(CC) $(CFLAGS) -c -o lexer/tokens.o lexer/tokens.c
parser/interface.o: parser/interface.c
	$(CC) $(CFLAGS) -c -o parser/interface.o parser/interface.c


#Test files
test: test/typedef/typedef.asm  

test/typedef/typedef.asm: test/typedef/typedef.c
	afcc/afcc > test/typedef/typedef.asm < test/typedef/typedef.c

#Clean
clean: 
	$(RM) afcc/afcc afcc/afcc.o  headers/limits.o  headers/float.o  lexer/tokens.o  parser/interface.o  

#Install
install: afcc/afcc
	$(CP) afcc/afcc /usr/local/bin
