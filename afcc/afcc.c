#include <stdio.h>
#include <stdlib.h>

#include "lexer/lexer.h"
#include "afcc/defines.h"

static void print_usage();

int main(int argc, char * argv[])
{
	FILE * fp_in, * fp_out;
	char * file_in, * file_out;
	int line_count = 0;
	
	/* if(argc < 2)
	{
		print_usage();
		return EXIT_FAILURE;
	} */
	
	/* Parse arguments function will go here... */
	
	/* For now, let's make our lives simple... */
	fp_out = stdout;
	fp_in = stdin;
	
	/* Parsing phase... (looks "scannerless" to the outside world) */
	parse_file(fp_in, NULL, NULL);
	
	
	
	return EXIT_SUCCESS;
}




void print_usage()
{
	fprintf(stderr, "afcc- ANSI Freestanding C Compiler\n" \
		"Usage: afcc [options] < infile > outfile\n");
	/* fprintf(stderr, "Options:\n");
	fprintf(stderr, "-c: Ignored for POSIX compatibility.\n"); */
}

