#include "afcc/defines.h"

/* http://www.mers.byu.edu/docs/standardC/limits.html */
char limits_header[]=
"#define CHAR_BIT   8"            _NL_ /* <#if expression >= 8>              */
"#define CHAR_MAX   127"          _NL_ /* <#if expression >= 127>            */
"#define CHAR_MIN   -128"         _NL_ /* <#if expression <= 0>              */
"#define INT_MAX    32767"        _NL_ /* <#if expression >= 32,767>         */
"#define INT_MIN    -32768"       _NL_ /* <#if expression <= -32,767>        */
"#define LONG_MAX   2147483647"   _NL_ /* <#if expression >= 2,147,483,647>  */
"#define LONG_MIN   -2147483648"  _NL_ /* <#if expression <= -2,147,483,647> */
"#define MB_LEN_MAX 1"            _NL_ /* <#if expression >= 1>              */
"#define SCHAR_MAX  127"          _NL_ /* <#if expression >= 127>            */
"#define SCHAR_MIN  -128"         _NL_ /* <#if expression <= -127>           */
"#define SHRT_MAX   32767"        _NL_ /* <#if expression >= 32,767>         */
"#define SHRT_MIN   -32768"       _NL_ /* <#if expression <= -32,767>        */
"#define UCHAR_MAX  255"          _NL_ /* <#if expression >= 255>            */
"#define UINT_MAX   65535"        _NL_ /* <#if expression >= 65,535>         */
"#define ULONG_MAX  4294967295"   _NL_ /* <#if expression >= 4,294,967,295>  */
"#define USHRT_MAX  65535"        _NL_; /* <#if expression >= 65,535>        */

