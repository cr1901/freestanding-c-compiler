#ifndef LEXER_H
#define LEXER_H

typedef enum token_type
{
	KEYWORD,
	IDENTIFIER,
	CONSTANT,
	STRING_LITERAL,
	OPERATOR,
	PUNCTUATOR,
	HEADER_NAME,
	PP_NUMBER,
	CHARACTER_CONSTANT,
	ALT_PREPROC_TOKEN, /* 3.1 Lexical Elements: each non-white-space 
		character that cannot be one of the above */
		
	/* Extra states to deal with edge cases */
	WHITESPACE_EOL, /* Buffer is line-based... whitespace to EOL very possible */
	UNDEFINED /* If a source character doesn't match... */
}TOKEN_TYPE;

typedef struct token
{
	TOKEN_TYPE type;
	char * value;
}TOKEN;

typedef struct lexer_state
{
	short int char_pos;
	/* SYMBOL_TABLE sym_table */	
}LEXER_STATE;   

TOKEN get_next_token(const char * line_buf, LEXER_STATE * state);
/* int get_next_token(const char * line_buf, TOKEN * next_token, LEXER_STATE * state); */
int find_next_non_ws_char(const char * line_buf, int offset);
int find_next_ws_char(const char * line_buf, int offset);
/* int try_keyword(const char * line_buf, int offset); */
int check_for_keyword(const char * line_buf);
#endif

