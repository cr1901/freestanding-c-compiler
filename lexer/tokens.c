#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>

#include "lexer/lexer.h"

/* keyword: one of

auto double int struct

break else long switch

case enum register typedef

char extern return union

const float short unsigned

continue for signed void

default goto sizeof volatile

do if static while */

/* 3.1.1 Semantics

 The above tokens (entirely in lower-case) are reserved (in translation phases 
 7 and 8) for use as keywords, and shall not be used otherwise.*/

const char * keywords[] = {"auto", "break", "case", \
	"char", "const", "continue", "default", "do", "double", "else", "enum", \
	"extern", "float", "for", "goto", "if", "int", "long", "register", \
	"return", "short", "signed", "sizeof", "static", "struct", "switch", \
	"typedef", "union", "unsigned", "void", "volatile"};
	
/* typedef enum lexer_state
{
	
}LEXER_STATE; */ /* "Wrong kind of tag" error- 
as opposed to redeclaration... hmm... */

/* 
   Trigraphs:
   ??=      #
   ??(      [
   ??/      \
   ??)      ]
   ??'      ^
   ??<      {
   ??!      |
   ??>      }
   ??-      ~ */
   
static char token_buf[512] = {'\0'};

   
TOKEN get_next_token(const char * line_buf, LEXER_STATE * state)
{
	int next_start_pos, next_max_end_pos; /* A token can ALWAYS end 
		before the next whitespace char. */
	TOKEN next_token = {UNDEFINED, token_buf};
	int token_identified = 0, offset = 0;
	
	next_start_pos = find_next_non_ws_char(line_buf, state->char_pos);
	next_max_end_pos = find_next_ws_char(line_buf, next_start_pos);
	
	if(next_start_pos == -1)
	{
		next_token.type = WHITESPACE_EOL;
		/* fprintf(stderr, "End of line (whitespace).\n"); */
		next_token.value = NULL;
		state->char_pos = 0;
		return next_token;
	}
	else if(next_max_end_pos == -1)
	{
		/* Replace with lexer_state->length or something... */
		/* warn() buffer full */
		/* fprintf(stderr, "End of line (no whitespace).\n"); */
		state->char_pos = 0; 
		next_max_end_pos = strlen(line_buf);
	}
	
	/* Signed-to-unsigned conversion is implementation-defined for negative numbers */
	assert(next_max_end_pos > next_start_pos);
	
	/* If we have a full token w/o a null terminator, copy it to the 
	buffer... */
	
	memcpy(token_buf, &line_buf[next_start_pos], \
		(unsigned int) next_max_end_pos - next_start_pos);
	token_buf[next_max_end_pos - next_start_pos] = '\0'; /* In worst case, this rewrites 
	null terminator already in token_buf. */
	
	state->char_pos = next_max_end_pos;
	/* fprintf(stderr, "%s: %d, %d\n", token_buf, next_start_pos, next_max_end_pos); */
	
	/* Do actual lexing here... */
	do
	{
		char curr_char = line_buf[offset];
		if(isupper(curr_char) || islower(curr_char) || curr_char == '_')
		{
			if(curr_char == 'L')
			{
				/* state = CHECK_FOR_WIDE_STRING */
			}
			else
			{
				if(check_for_keyword(&line_buf[offset]))
				{
					token_identified = 1;
				}
				else
				{
					/* state = MUNCH_IDENTIFIER */
				}
			}
		}
		token_identified = 1;
		offset++;
	}while(!token_identified && offset < next_max_end_pos);
	/* offset = next_start_pos;
	while(0)
	{
		char curr_char = line_buf[offset];
		int next_pos;
		if(isupper(curr_char) || islower(curr_char) || curr_char == '_')
		{
			
		}	
	} */
	
	return next_token;
}

int find_next_non_ws_char(const char * line_buf, int offset)
{
	while(isspace(line_buf[offset]) && line_buf[offset] != '\0')
	{
		offset++;
	}
	
	return (line_buf[offset] == '\0') ? -1 : offset;
}

int find_next_ws_char(const char * line_buf, int offset)
{
	while(!isspace(line_buf[offset]) && line_buf[offset] != '\0')
	{
		offset++;
	}
	
	return (line_buf[offset] == '\0') ? -1 : offset;
}

/* Only makes sense for the first character in a potential token. This is an 
extremely naive- and hopefully temporary- solution. */
int check_for_keyword(const char * line_buf)
{
	unsigned int count;
	int keyword_found = 0;
	for(count = 0; count < sizeof(keywords); count++)
	{
		keyword_found = !strcmp(line_buf, keywords[count]);
		if(keyword_found)
		{
			break;
		}
	}
	
	return keyword_found;
}
