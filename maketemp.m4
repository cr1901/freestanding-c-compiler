include(`F:/Projects/m4_tut/genmake.m4')
_DEFINE_MACROS
define(AFCC_SRCS, `afcc/afcc, headers/limits, headers/float, lexer/tokens, parser/interface')dnl
define(TEST_SRCS, `test/typedef/typedef')dnl
define(_CCTEST, `$1.$3: $1.$2
	afcc/afcc > $1.$3 < $1.$2
')dnl

#Executable Link
_LINKOBJ(`AFCC_SRCS', `afcc/afcc', objext)

#C Sources- AFCC
nospaceforeach(`X', _BUILDSRC(`X', `c', objext), AFCC_SRCS)dnl

dnl
dnl #ASM Sources
dnl ifdef(asmsrcs, nospaceforeach(`X', _BUILDSRC(`X', `asm', objext), asmsrcs))dnl

#Test files
test: _APPENDEXT(TEST_SRCS, `asm')

nospaceforeach(`X', _CCTEST(`X', `c', `asm'), TEST_SRCS)dnl

#Clean
_CLEAN(``afcc/afcc' _APPENDEXT(`AFCC_SRCS', `objext')')

#Install
_INSTALL(outname, `/usr/local/bin')
