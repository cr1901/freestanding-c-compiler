#include <stdio.h>

#include "parser/parser.h"
#include "lexer/lexer.h"

static char line_buffer[512] = {'\0'}; /* Likely to become extern as files are
split by purpose. */

/* Internally, the parser is divided into lexer/parser, but the exposed inteface
looks scannerless. */
int parse_file(FILE * fp_in, AST * astree, SYMBOL_TABLE * symtab)
{
	LEXER_STATE lex_state;
	
	lex_state.char_pos = 0;
	while((fgets(line_buffer, 512, fp_in) != NULL))
	{
		TOKEN new_token;
		
		/* fputs("Retrieved new line.\n", stderr); */
		do
		{
			new_token = get_next_token(line_buffer, &lex_state);
			/* We need a check_for_newline function:
			2.2.1 Character sets-
			In source files, there shall be some way of indicating the end of 
			each line of text; this Standard treats such an end-of-line 
			indicator as if it were a single new-line character.*/ 
			fputs(new_token.value, stdout);
			fputs("\n", stdout);
		}while(new_token.type != WHITESPACE_EOL);
		
		/* File that does not end in newline is technically undefined...
		need to look up why that is so (when fgets won't choke on this) */
	}
	
	return 0;
}

