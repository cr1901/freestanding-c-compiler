#ifndef PARSER_H
#define PARSER_H

#include "ast/ast.h"
#include "symbol/symbol.h"

int parse_file(FILE * fp, AST * astree, SYMBOL_TABLE * symtab);
#endif

