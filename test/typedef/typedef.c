typedef int my_new_type_t;

/* http://ideone.com/e1if3y */
void lexer_hack_test()
{
	my_new_type_t int_t1, int_t2;
	my_new_type_t (int_t3); /* Function call or variable declaration?- Declaration */
	my_new_type_t * p_int; /* Multiplication or variable declaration?- Declaration */
	(my_new_type_t) * p_int; /* Multiplication or typecast?- Typecast */
	my_new_type_t my_new_type_t; /* Legal... typedef can be mask inside scope. */
	
	my_new_type_t = 0;
	int_t1 = 1;
	int_t2 = 2;
	int_t3 = 3;
	lexer_hack_fun (int_t3); /* Function call or variable declaration?- Function call */
	int_t1 * int_t2; /* Multiplication or variable declaration?- Multiplication */
	int_t2 = my_new_type_t * int_t1; /* Typedef will be masked now. */
	p_int = &int_t3;
}

/* Also demonstrates equivalence of typedefs to the original type. */
int lexer_hack_fun(int t)
{
	return t;
}
